﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Experimental.UIElements;

public class ShoppingList : MonoBehaviour {
    public List<Vector3> pointsToFollow;
    public PathDraw pathDraw;

    // Use this for initialization
    void Start() {
        pathDraw = GameObject.Find("PathDrawer").GetComponent<PathDraw>();
    }

    // Update is called once per frame
    void Update() {

    }

    public void UpdateTargets() {
        pointsToFollow.OrderBy(v => v.x).ToList();
        pathDraw.targets = pointsToFollow;
    }
}
