﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckBox : MonoBehaviour {
    public Toggle toggle;
    public ShoppingList shoppingListRef;
    public Text text;
    public GameObject target;

	// Use this for initialization
	void Start () {
        shoppingListRef = GameObject.Find("ShoppingList").GetComponent<ShoppingList>();
        toggle = GetComponent<Toggle>();

        toggle.onValueChanged.AddListener((value) => {
            HandleToggle(value);
        });
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void HandleToggle(bool value) {
        if (value)
        {
            shoppingListRef.pointsToFollow.Add(target.transform.position);
            shoppingListRef.UpdateTargets();
        }
        else
        {
            shoppingListRef.pointsToFollow.Remove(target.transform.position);
            shoppingListRef.UpdateTargets();
        }
    }
}
