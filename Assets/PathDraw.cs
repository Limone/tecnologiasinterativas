﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PathDraw : MonoBehaviour {

    public List<Vector3> targets;
    public float minDist;
    private NavMeshPath path;
    private LineRenderer line;
    private Vector3 prevPosition;
    private int totalPositionsCount;

    void Start() {
        path = new NavMeshPath();
        line = GetComponent<LineRenderer>();
        line.alignment = LineAlignment.TransformZ;
    }

    void Update() {
        // Update the way to the goal every second.
        if (targets.Count > 0) {
            if (Vector3.Distance(transform.position, targets[0]) <= minDist) {
                targets.RemoveAt(0);
            }
            totalPositionsCount = 0;
            line.positionCount = 0;
            prevPosition = transform.position;
            foreach (Vector3 target in targets) {
                NavMesh.CalculatePath(new Vector3(prevPosition.x, -2, prevPosition.z), new Vector3(target.x, -2, target.z), NavMesh.AllAreas, path);
                line.positionCount += path.corners.Length;
                for (int i = 0; i < path.corners.Length; i++) {
                    line.SetPosition(i + totalPositionsCount, path.corners[i]);
                }
                totalPositionsCount += path.corners.Length;
                prevPosition = target;
            }
        }
    }
}
