﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MarketController : MonoBehaviour {
    public static MarketController instance;

    public static MarketController Instance { get { return instance; } }

    public string imageName;

    private Vector3[] positionMap = { new Vector3 (-15f,1.5f,0),
                                        new Vector3 (8f,1.5f,0),
                                        new Vector3 (-33f, 1.5f, 0f)};

    private Vector3[] rotationMap = {
        new Vector3(0,0,0),
        new Vector3(0, 90, 0),
        new Vector3(0, 180, 0)
    };

    public GameObject player;
    public Text debugText;
    public Text debugText2;

    // Use this for initialization
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
    }

    public void HandleImageDetection(int imageIndex)
    {
        player.transform.position = positionMap[imageIndex];
        player.transform.rotation = Quaternion.Euler(rotationMap[imageIndex]);
        debugText.text = imageIndex.ToString();
    }

    public void DebugText(int imageIndex) {
        debugText2.text = imageIndex.ToString();
    }
   

}
